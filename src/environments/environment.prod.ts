export const environment = {
  production: true,
  apiUrl: "https://20742d30-d4b5-4d20-857d-42cf7990dda9.mock.pstmn.io/api",
  enableGuards: true
};
