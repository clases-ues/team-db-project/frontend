import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from '@modules/auth/login/login.component';
import { NewOrderComponent } from '@modules/dashboard/new-order/new-order.component';
import { OrderDetailsComponent } from '@modules/dashboard/order-details/order-details.component';
import { OrdersListComponent } from '@modules/dashboard/orders-list/orders-list.component';
import { SkeletonComponent } from '@modules/dashboard/skeleton/skeleton.component';
import { SessionService } from '@shared/services/guards/session.service';

const routes: Routes = [
  { path: "auth/login", component: LoginComponent},
  {
    path: "dashboard", component: SkeletonComponent, canActivate: [SessionService],
    children: [
      { path: 'new-order', component: NewOrderComponent, canActivate: [SessionService] },
      { path: 'orders', component: OrdersListComponent, canActivate: [SessionService] },
      { path: 'orders/:orderId', component: OrderDetailsComponent, canActivate: [SessionService]},
      { path: '', pathMatch: 'prefix', redirectTo: 'orders' }
    ]
  },
  {
    path: '', pathMatch: 'prefix', redirectTo: 'dashboard'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
