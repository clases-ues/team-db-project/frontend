import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { RoutePaths } from '@data/constants/route-paths.enum';
import { SuppliesApiService } from '@shared/services/apis/supplies/api.service';
import { LocalStorageService } from '@shared/services/storage/local-storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {

  loginForm = new FormGroup({
    nickname: new FormControl(''),
    password: new FormControl('')
  })

  disableLogin: boolean = true

  constructor(
    private suppliesApi: SuppliesApiService, 
    private storage: LocalStorageService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  onSubmit() {
    this.suppliesApi.login(this.loginForm.value).subscribe(
      response => {
        const { token } = response as {token: string}
        this.storage.token(token)
        this.router.navigate([RoutePaths.DASHBOARD])
      }, 
      error => {
        alert('Ha habido un error intenta otra vez')
      })
  }
}
