import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OrderStatus } from '@data/constants/order-status.enum';
import { RoutePaths } from '@data/constants/route-paths.enum';
import { SuppliesApiService } from '@shared/services/apis/supplies/api.service';
import { Order, OrderHistory, OrderItem } from '@shared/services/apis/supplies/interface/orders.interface';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.sass']
})
export class OrderDetailsComponent implements OnInit {
  order!: Order;
  orderItems!: OrderItem[]
  orderHistory!: OrderHistory[]

  constructor(
    private currentRoute: ActivatedRoute, 
    private suppliersApi: SuppliesApiService,
    private router: Router
    ) { }

  ngOnInit(): void {
    this.currentRoute.params.subscribe(params => {
      const {orderId} = params
      this.loadOrderDetails(orderId)
    })
  }

  loadOrderDetails (orderId: number) {
    this.suppliersApi.getOrderDetails(orderId).subscribe(
      response => {
        console.log(`Order details:`, response)
        this.order = response
        this.orderItems = response.insumo || []
        this.orderHistory = response.historial || []
      },
      error => {
        console.error(error)
      }
    )
  }

  getIconStatus (statusId: number): string {
    const iconName = statusId == OrderStatus.CANCELLED ? 'report_problem' : 'check_circle'
    return iconName
  }

  canEditStatus () {
    const orderStatus = this.order.estado.estadoPedidoId
    const isEditable = !(orderStatus == OrderStatus.RECEIVED || orderStatus == OrderStatus.CANCELLED)
    return isEditable;
  }

  onOrderCancel () {
    const payload = {
      orderId: this.order.pedidoId,
      statusId: OrderStatus.CANCELLED
    }
    this.suppliersApi.updateOrderStatus(payload).subscribe(
      response => {
        alert('Orden Cancelada')
        this.loadOrderDetails(this.order.pedidoId)
      }
    )
  }

  onOrderReceived () {
    const payload = {
      orderId: this.order.pedidoId,
      statusId: OrderStatus.RECEIVED
    }
    this.suppliersApi.updateOrderStatus(payload).subscribe(
      response => {
        alert('Orden completada')
        this.loadOrderDetails(this.order.pedidoId)
      }
    )
  }
}
