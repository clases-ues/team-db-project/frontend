import { NgModule } from '@angular/core';
import { SkeletonComponent } from './skeleton/skeleton.component';
import { SharedModule } from '@shared/shared.module';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { NewOrderComponent } from './new-order/new-order.component';
import { OrdersListComponent } from './orders-list/orders-list.component';
import { OrderItemComponent } from './order-item/order-item.component';
import { OrderDetailsComponent } from './order-details/order-details.component';



@NgModule({
  declarations: [
    SkeletonComponent, 
    NewOrderComponent, 
    OrdersListComponent, 
    OrderItemComponent, OrderDetailsComponent],
  imports: [
    SharedModule  ,
    DashboardRoutingModule  
  ]
})
export class DashboardModule { }
