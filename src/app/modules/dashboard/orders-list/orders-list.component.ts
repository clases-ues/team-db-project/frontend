import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { RoutePaths } from '@data/constants/route-paths.enum';
import { OrderTable } from '@data/interfaces/order.interface';
import { SuppliesApiService } from '@shared/services/apis/supplies/api.service';
import { Order } from '@shared/services/apis/supplies/interface/orders.interface';

@Component({
  selector: 'app-orders-list',
  templateUrl: './orders-list.component.html',
  styleUrls: ['./orders-list.component.sass']
})
export class OrdersListComponent implements OnInit {
  orders!: Order[]
  dataSource!: MatTableDataSource<OrderTable>
  displayedColumns: string[] = ['id', 'supplier', 'status', 'createdAt', 'receptionDate', 'orderCost', 'transportCost', 'total']

  constructor(
    private suppliesApi: SuppliesApiService, 
    private router: Router) { }

  ngOnInit(): void {
    this.suppliesApi.getOrdersList().subscribe(
      response => {
        this.orders = response
        const items: OrderTable[] = this.getOrdersAsTable()
        this.dataSource = new MatTableDataSource(items)
      },
      error => {
        console.log(error)
        alert('Ha ocurrido un error')
      }
    )
  }

  getOrdersAsTable (): OrderTable [] {
    const tableItems = this.orders.map(order => {
      const item: OrderTable = {
        id: order.pedidoId,
        supplier: order.proveedor.nombre,
        status: order.estado.estado,
        createdAt: order.fechaCreacion,
        receptionDate: order.fechaEntrega,
        orderCost: order.costoPedido,
        transportCost: order.costoTransporte,
        total: order.total
      }
      return item
    })

    return tableItems;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onOrderClicked (order: OrderTable) {
    this.router.navigate([RoutePaths.ORDERS, order.id])
  }
}
