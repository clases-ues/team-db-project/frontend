import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { navigationItems } from "../../../data/constants/dashboard-menu";
import { LocalStorageService } from '@shared/services/storage/local-storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-skeleton',
  templateUrl: './skeleton.component.html',
  styleUrls: ['./skeleton.component.css']
})
export class SkeletonComponent {
  navigationItems = navigationItems

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(
    private breakpointObserver: BreakpointObserver,
    private storage: LocalStorageService,
    private router: Router
  ) {}
  
  logout() {
    this.storage.reset()
    this.router.navigate(['/auth/login'])
  }
}
