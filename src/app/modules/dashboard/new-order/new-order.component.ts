import { Component, OnInit } from '@angular/core';

import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { RoutePaths } from '@data/constants/route-paths.enum';
import { OrderCreateDto, ProductsListDto } from '@shared/services/apis/supplies/api.dto';
import { SuppliesApiService } from '@shared/services/apis/supplies/api.service';
import { Order, OrderItem } from '@shared/services/apis/supplies/interface/orders.interface';
import { Product } from '@shared/services/apis/supplies/interface/products.interface';
import { Supplier } from '@shared/services/apis/supplies/interface/suppliers.interface';

@Component({
  selector: 'app-new-order',
  templateUrl: './new-order.component.html',
  styleUrls: ['./new-order.component.sass']
})
export class NewOrderComponent implements OnInit {
  
  orderForm = new FormGroup({
    providerId: new FormControl(''),
    receptionDate: new FormControl(''),
    productId: new FormControl(''),
    quantity: new FormControl(5),
  })

  transportCost: number = 0
  providers: Supplier[] = []
  products: Product[] = []
  
  selectedProvider!: Supplier
  selectedProduct?: Product | null
  selectedProductQuantity: number = 0
  orderItems: OrderItem[] = []

  constructor(private suppliesApi: SuppliesApiService, private router: Router) { }

  ngOnInit(): void {
    this.loadProviders()
  }

  loadProviders() {
    this.suppliesApi.getSuppliersList().subscribe(
      response => {
        this.providers = response
      },
      error => {
        console.log(error)
      }
    )
  }

  loadProviderProducts () {
    const filter: ProductsListDto = {
      supplierId: this.selectedProvider?.proveedorId || 0
    }
    this.suppliesApi.getProductsList(filter).subscribe(
      response => {
        console.log(response)
        this.products = response
        this.orderItems = []
      },
      error => {
        console.log(error)
      }
    ) 
  }

  clearProductSelection () {
    this.selectedProduct = null
    let newFormValue = {
      ...this.orderForm.value,
      productId: 0
    }
    newFormValue.productId = 0
    this.orderForm.reset(newFormValue)
  }

  getTotalOrder (): number {
    const totalItemsCost = this.orderItems.reduce((prev: number, curr) => {
      return prev + curr.total;
    }, 0)
    return totalItemsCost + this.transportCost
  }

  onSubmit () {
    const items = this.orderItems.map(item => {
      return {
        insumoId: item.insumoId,
        cantidad: item.cantidad,
        precio: item.precio
      }
    })
    const orderPayload: OrderCreateDto = {
      proveedorId: this.selectedProvider.proveedorId,
      fechaEntrega: this.orderForm.value.receptionDate,
      insumos: items
    }

    this.suppliesApi.createOrder(orderPayload).subscribe(
      response => {
        console.log(response)
        alert('pedido enviado')
        this.router.navigate([RoutePaths.ORDERS])
      },
      error => {
        console.log(error);
        alert('Error en la solicitud')
      }
    )
  }

  onProviderSelectChange (id: number) {
    const [provider] = this.providers.filter( provider => {
      return provider.proveedorId == id
    })
    this.selectedProvider = provider
    this.transportCost = this.selectedProvider.costoTransporte
    this.loadProviderProducts();
  }

  onProductSelectChange (id: number) {
    const [product] = this.products.filter( product => {
      return product.insumoId == id
    })
    this.selectedProduct = product

    // console.log(this.orderForm.value)
  }

  onAddProductClicked () {
    if (this.selectedProduct) {
      const quantity = this.orderForm.value.quantity
      const total = quantity * this.selectedProduct.precio
      const newItem: OrderItem = {
        ...this.selectedProduct,
        cantidad: this.orderForm.value.quantity,
        total: total
      }
      this.orderItems.push(newItem)
      this.clearProductSelection()
    }
  }


}
