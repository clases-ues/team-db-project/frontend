import { Component, Input, OnInit } from '@angular/core';
import { OrderItem } from '@shared/services/apis/supplies/interface/orders.interface';

@Component({
  selector: 'app-order-item',
  templateUrl: './order-item.component.html',
  styleUrls: ['./order-item.component.sass']
})
export class OrderItemComponent implements OnInit {
  @Input() item!: OrderItem

  displayedColumns: string [] = ['insumoId', "nombre", "precio", "cantidad", "total"];
  constructor() { }

  ngOnInit(): void {
  }
}
