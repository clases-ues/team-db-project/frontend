export interface OrderTable {
  id: number;
  supplier: string;
  status: string;
  createdAt: string;
  receptionDate: string;
  orderCost: number; 
  transportCost: number;
  total: number;
}