import { RoutePaths } from "./route-paths.enum";

interface MenuItem {
  label: string;
  path: string;
}

const navigationItems: MenuItem[] = [
  {
    label: 'nuevo pedido',
    path: RoutePaths.NEW_ORDER
  },
  {
    label: 'pedidos',
    path: RoutePaths.ORDERS
  }
]

export {
  navigationItems
}