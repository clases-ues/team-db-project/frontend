export enum OrderStatus {
  SENDED = 1,
  APPROVED = 2,
  IN_ROUTE = 3,
  CANCELLED = 4,
  RECEIVED = 5
}