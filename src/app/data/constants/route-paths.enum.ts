export enum RoutePaths {
  LOGIN = '/auth/login/',
  DASHBOARD = '/dashboard',
  ORDERS = '/dashboard/orders/',
  NEW_ORDER = '/new-order/',
  ORDER_HISTORY = '/dashboard/order-history/'
}