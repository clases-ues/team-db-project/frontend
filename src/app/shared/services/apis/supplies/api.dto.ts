import { OrderItem } from "./interface/orders.interface";

interface UserLoginDto {
  nickname: string;
  password: string;
}

interface OrderCreateItem extends Partial<OrderItem> {
  insumoId: number;
  cantidad: number;
}

interface OrderCreateDto {
  proveedorId: number;
  fechaEntrega: string;
  sucursalId?: number;
  insumos: OrderCreateItem[];
}

interface ProductsListDto {
  categories?: number[];
  supplierId: number;
}

export {
  UserLoginDto,
  OrderCreateDto,
  ProductsListDto
}