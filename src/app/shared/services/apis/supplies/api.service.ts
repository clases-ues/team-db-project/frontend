import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { LocalStorageService } from '../../storage/local-storage.service';
import { OrderCreateDto, ProductsListDto, UserLoginDto } from './api.dto';
import { ProductCategory } from './interface/categories.interface';
import { OrderStatus } from './interface/order-status.interface';
import { Product } from './interface/products.interface';
import { Supplier } from './interface/suppliers.interface';
import { Order } from './interface/orders.interface'

interface HttpOptions {
  headers: HttpHeaders;
  params?: any
}

@Injectable({
  providedIn: 'root'
})
export class SuppliesApiService {

  private apiUrl = environment.apiUrl
  

  constructor(private http: HttpClient, private storage: LocalStorageService) { }

  login (credentials: UserLoginDto) {
    return this.http.post(`${this.apiUrl}/usuario/autenticar`, credentials)
  }

  private getHttpOptions (params = {}): HttpOptions {
    const token = this.storage.token()
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      }),
      params
    } 
    return httpOptions
  }

  getCategoryList () {
    const options = this.getHttpOptions()
    return this.http.get<ProductCategory[]>(`${this.apiUrl}/categoria`, options);
  }

  getSuppliersList () {
    const httpOptions = this.getHttpOptions()
    return this.http.get<Supplier[]>(`${this.apiUrl}/proveedor`, httpOptions);
  }

  getProductsList (options: ProductsListDto) {
    const { supplierId, categories } = options
    const params = {
      categories: categories?.length ? categories.join(',') : ''
    }
    const httpOptions = this.getHttpOptions(params)
    return this.http.get<Product[]>(`${this.apiUrl}/proveedor/${supplierId}/insumo`, httpOptions);
  }

  getOrderStatusList () {
    const httpOptions = this.getHttpOptions()
    return this.http.get<OrderStatus[]>(`${this.apiUrl}/pedido/estado`, httpOptions);
  }

  getOrdersList () {
    const httpOptions = this.getHttpOptions()
    return this.http.get<Order[]>(`${this.apiUrl}/pedido`, httpOptions);
  }

  getOrderDetails (orderId: number) {
    const httpOptions = this.getHttpOptions()
    return this.http.get<Order>(`${this.apiUrl}/pedido/${orderId}`, httpOptions);
  }

  getOrderHistory (orderId: number) {
    const httpOptions = this.getHttpOptions()
    return this.http.get<any[]>(`${this.apiUrl}/pedido/${orderId}/historial`, httpOptions);
  }

  updateOrderStatus (options: {orderId: number; statusId: number}) {
    const {orderId, statusId} = options
    const httpOptions = this.getHttpOptions()
    return this.http.patch<Order>(`${this.apiUrl}/pedido/${orderId}/estado/${statusId}`, {},  httpOptions);
  }

  createOrder (payload: OrderCreateDto) {
    const httpOptions = this.getHttpOptions()
    const userData = this.storage.userdata()
    payload.sucursalId = userData.sucursalId;
    return this.http.post<Order>(`${this.apiUrl}/pedido`, payload,  httpOptions);
  }
}
