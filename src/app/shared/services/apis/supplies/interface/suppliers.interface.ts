interface Supplier {
  proveedorId: number;
  nombre: string;
  descripcion: string;
  fechaCreacion: string;
  costoTransporte: number;
}

export {
  Supplier
}