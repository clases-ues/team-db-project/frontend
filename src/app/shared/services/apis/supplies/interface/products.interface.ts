interface Product {
  insumoId: number;
  nombre: string;
  descripcion: string;
  precio: number;
}

export {
  Product
}