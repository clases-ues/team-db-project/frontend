export interface User {
  usuarioId: number,
  nickname: string,
  email: string,
  rol: string,
  sucursalId: 1,
  sucursal: {
    sucursalId: 1,
    nombre: string,
    fechaCreacion: string,
    direccion: string,
    paisId: number,
    pais: string
  }
}
