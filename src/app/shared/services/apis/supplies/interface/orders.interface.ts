import { OrderStatus } from "./order-status.interface";
import { Product } from "./products.interface";

interface OrderItem extends Product {
  cantidad: number
  total: number
}

interface OrderHistory extends OrderStatus {
  fechaCreacion: string
}

interface Order {
  pedidoId: number;
  proveedor: {
    proveedorId: number,
    nombre: string
  },
  sucursal: {
      sucursalId: number,
      nombre: string
  },
  estado: {
      estadoPedidoId: number,
      estado: string
  },
  historial?: OrderHistory[]
  insumo?: OrderItem[]
  fechaCreacion: string,
  fechaEntrega: string,
  costoPedido: number,
  costoTransporte: number,
  total: number
}

export {
  Order,
  OrderItem,
  OrderHistory
}