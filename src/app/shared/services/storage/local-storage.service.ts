import { Injectable } from '@angular/core';
import { User } from '../apis/supplies/interface/user.interface';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor() { }

  reset(){
    localStorage.clear();
    this.setDefaults();
  }

  isDefaultSet(){
    return localStorage.getItem("defaultSet") !== "true";
  }

  setDefaults(){
    // TODO: Set default properties

    localStorage.setItem("defaultSet", "true");
  }

  token(token?: string): any{
    if( !token ){
      return localStorage.getItem("accessToken");
    } else {
      localStorage.setItem("accessToken", token);
    }
  }

  userdata(): User{
    let token: string = this.token();

    let data = token.split(".");

    return JSON.parse( atob(data[1]) );
  }

  isLoggedIn(): boolean {
    const isAccessTokenDefined = localStorage.getItem("accessToken") !== null
    const isAccessTokenNotEmpty = localStorage.getItem("accessToken") !== ''
    const isAccessTokenSaved = isAccessTokenDefined && isAccessTokenNotEmpty
    return isAccessTokenSaved;
  }
}
