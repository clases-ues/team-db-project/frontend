import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { LocalStorageService } from '../storage/local-storage.service';

import { environment } from "../../../../environments/environment";
import { RoutePaths } from "../../../data/constants/route-paths.enum";

@Injectable({
  providedIn: 'root'
})
export class SessionService implements CanActivate {

  constructor(private router: Router, private storage: LocalStorageService) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean
  {
    const isUserLogged = this.storage.isLoggedIn()
    const isGuardDisabled = !environment.enableGuards

    if (isUserLogged || isGuardDisabled) {
      return true;
    }

    this.router.navigate([RoutePaths.LOGIN])
    return false;
  }
}
