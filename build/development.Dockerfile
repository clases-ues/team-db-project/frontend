FROM node:14-alpine as build-step

# Se crea el directorio de la aplicación
WORKDIR /usr/src/app

# Se instalan las dependencias de la aplicación
# Se usa un wildcard para asegurar que tanto el package.json y el package-lock.json sean copiados
# si aplica (npm@5+)
COPY ./ /usr/src/app/

RUN npm install

# run npm install @angular/cli

EXPOSE 4200
EXPOSE 9876

CMD [ "npm", "start" ]
