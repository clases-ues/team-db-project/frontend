FROM node:14-alpine as build-step

ARG ENV=prod

# Se crea el directorio de la aplicación
WORKDIR /usr/src/app

# Se instalan las dependencias de la aplicación
# Se usa un wildcard para asegurar que tanto el package.json y el package-lock.json sean copiados
# si aplica (npm@5+)
COPY ./ /usr/src/app/

RUN npm install

# run npm install @angular/cli

RUN npm run build:${ENV}

# Use official nginx image as the base image
FROM nginx:1.21.1-alpine

#custom nginx conf
COPY nginx.conf /etc/nginx/conf.d/default.conf

# Copy the build output to replace the default nginx contents.

COPY --from=build-step /usr/src/app/dist/flash-onboarding/ /usr/share/nginx/html/

RUN echo $(ls -al)

EXPOSE 80

# docker build . -t registry.gitlab.com/unchectorguzman/flash-onboarding-front:1.0-alpha
# docker push registry.gitlab.com/unchectorguzman/flash-onboarding-front:1.0-alpha
# docker run  -d -p 8080:80 registry.gitlab.com/unchectorguzman/flash-onboarding-front:1.0-alpha
